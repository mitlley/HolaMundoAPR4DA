package inacap.test.holamundoapr4da.controller;

import android.content.ContentValues;
import android.content.Context;

import inacap.test.holamundoapr4da.model.sqlite.HolaMundoDBContract;
import inacap.test.holamundoapr4da.model.sqlite.UsuariosModel;

/**
 * Created by mitlley on 01-09-17.
 */

public class UsuariosController {

    // Mediante este objeto accedemos a la Base de Datos
    private UsuariosModel usuariosModel;

    public UsuariosController(Context context) {
        this.usuariosModel = new UsuariosModel(context);
    }

    public void crearUsuario(String username, String password1, String password2) throws Exception{

        if(!password1.equals(password2)){
            // SI las contraseñas no coinciden lanzamos un Exception
            // Este debe ser manejado por la Activity

            throw new Exception("Contraseñas no coinciden");
        }

        // Llamar a usuarioModelo para que almacene en la base de datos
        ContentValues usuario = new ContentValues();
        usuario.put(HolaMundoDBContract.HolaMundoUsuario.COLUMN_NAME_USERNAME, username);
        usuario.put(HolaMundoDBContract.HolaMundoUsuario.COLUMN_NAME_PASSWORD, password1);

        this.usuariosModel.crearUsuario(usuario);
    }

    public boolean usuarioLogin(String username, String password){
        // Pedir los datos del usuario
        ContentValues usuario = this.usuariosModel.obtenerUsuarioPorUsername(username);

        if(usuario == null){
            return false;
        }

        if(password.equals(usuario.get(HolaMundoDBContract.HolaMundoUsuario.COLUMN_NAME_PASSWORD))){
            return true;
        }

        return false;
    }

}





















