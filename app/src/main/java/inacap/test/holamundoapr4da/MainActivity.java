package inacap.test.holamundoapr4da;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import inacap.test.holamundoapr4da.controller.UsuariosController;
import inacap.test.holamundoapr4da.model.sqlite.HolaMundoDBContract;
import inacap.test.holamundoapr4da.views.FormularioActivity;
import inacap.test.holamundoapr4da.views.Home2Activity;

public class MainActivity extends AppCompatActivity {

    private EditText editTextUsername, editPassword;
    private Button buttonLogin;
    private TextView textViewUsername, tvCrearCuenta;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SharedPreferences sesion = getSharedPreferences(HolaMundoDBContract.HolaMundoSesion.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        boolean loggedIn = sesion.getBoolean(HolaMundoDBContract.HolaMundoSesion.FIELD_SESSION, false);
        if(loggedIn){
            Intent i = new Intent(MainActivity.this, Home2Activity.class);
            startActivity(i);
            finish();
        }


        this.editTextUsername = (EditText) findViewById(R.id.etUsername);
        this.editPassword = (EditText) findViewById(R.id.etPassword);

        this.buttonLogin = (Button) findViewById(R.id.btLogin);
        this.textViewUsername = (TextView) findViewById(R.id.tvUsername);
        this.tvCrearCuenta = (TextView) findViewById(R.id.tvCrearCuenta);

       // Listener que detecte el click sobre el boton
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Tomar los datos ingresados por el usuario
                String username = editTextUsername.getText().toString();
                String password = editPassword.getText().toString();

                UsuariosController controller = new UsuariosController(getApplicationContext());
                if(controller.usuarioLogin(username, password)){
                    Intent intent = new Intent(MainActivity.this, Home2Activity.class);
                    startActivity(intent);


                    // GUardar el inicio de sesion
                    SharedPreferences sesion = getSharedPreferences(HolaMundoDBContract.HolaMundoSesion.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sesion.edit();

                    editor.putBoolean(HolaMundoDBContract.HolaMundoSesion.FIELD_SESSION, true);
                    editor.putString(HolaMundoDBContract.HolaMundoSesion.FIELD_USERNAME, username);

                    editor.commit();


                    finish();
                } else {
                    Toast.makeText(getApplicationContext(), "Datos incorrectos", Toast.LENGTH_SHORT).show();
                }
            }
        });

        this.tvCrearCuenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Abrir la segunda ventana
                Intent nuevaVentana = new Intent(MainActivity.this, FormularioActivity.class);
                startActivity(nuevaVentana);
            }
        });


    }
}
