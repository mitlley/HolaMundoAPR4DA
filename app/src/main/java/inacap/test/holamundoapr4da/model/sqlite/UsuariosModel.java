package inacap.test.holamundoapr4da.model.sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by mitlley on 25-08-17.
 */

public class UsuariosModel {
    private HolaMundoDBHelper dbHelper;

    public UsuariosModel(Context context) {
        this.dbHelper = new HolaMundoDBHelper(context);
    }

    public void crearUsuario(ContentValues usuario){
        // Crear conexion con la base de datos
        SQLiteDatabase db = this.dbHelper.getWritableDatabase();

        db.insert(HolaMundoDBContract.HolaMundoUsuario.TABLE_NAME, null, usuario);
    }

    public ContentValues obtenerUsuarioPorUsername(String username){
        // Solicitamos una conexion a la base de datos
        SQLiteDatabase db = this.dbHelper.getReadableDatabase();

        // Definir que columnas nos interesan
        String[] projection = {
                HolaMundoDBContract.HolaMundoUsuario._ID,
                HolaMundoDBContract.HolaMundoUsuario.COLUMN_NAME_USERNAME,
                HolaMundoDBContract.HolaMundoUsuario.COLUMN_NAME_PASSWORD
        };

        String selection = HolaMundoDBContract.HolaMundoUsuario.COLUMN_NAME_USERNAME + " = ? LIMIT 1";
        String[] selectionArgs = { username };

        // Hacemos la consulta
        Cursor cursor = db.query(
                HolaMundoDBContract.HolaMundoUsuario.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null, null, null
        );

        if(cursor.getCount() == 0) return null;

        // Nos movemos al primer resultado
        cursor.moveToFirst();

        // Pedir los datos al cursor
        String cursor_username = cursor.getString(cursor.getColumnIndex(HolaMundoDBContract.HolaMundoUsuario.COLUMN_NAME_USERNAME));
        String cursor_password = cursor.getString(cursor.getColumnIndex(HolaMundoDBContract.HolaMundoUsuario.COLUMN_NAME_PASSWORD));

        ContentValues usuario = new ContentValues();
        usuario.put(HolaMundoDBContract.HolaMundoUsuario.COLUMN_NAME_USERNAME, cursor_username);
        usuario.put(HolaMundoDBContract.HolaMundoUsuario.COLUMN_NAME_PASSWORD, cursor_password);

        return usuario; // Esto debemos remplazarlo
    }
}















