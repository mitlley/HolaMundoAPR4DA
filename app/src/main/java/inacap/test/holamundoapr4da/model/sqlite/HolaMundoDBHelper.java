package inacap.test.holamundoapr4da.model.sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by mitlley on 25-08-17.
 */

public class HolaMundoDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "HolaMundo.db";
    public static final int DATABASE_VERSION = 1;

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + HolaMundoDBContract.HolaMundoUsuario.TABLE_NAME +
            "(" + HolaMundoDBContract.HolaMundoUsuario._ID + " INTEGER PRIMARY KEY," +
                    HolaMundoDBContract.HolaMundoUsuario.COLUMN_NAME_USERNAME + " TEXT," +
                    HolaMundoDBContract.HolaMundoUsuario.COLUMN_NAME_PASSWORD + " TEXT)";

    public HolaMundoDBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}












