package inacap.test.holamundoapr4da.model.sqlite;

import android.provider.BaseColumns;

/**
 * Created by mitlley on 25-08-17.
 */

public class HolaMundoDBContract {
    private HolaMundoDBContract() {}

    /* Columnas de la tabla Usuarios */
    public static class HolaMundoUsuario implements BaseColumns {
        public static final String TABLE_NAME = "usuarios";
        public static final String COLUMN_NAME_USERNAME = "username";
        public static final String COLUMN_NAME_PASSWORD = "password";
    }

    public static class HolaMundoSesion {
        public static final String SHARED_PREFERENCES_NAME = "sesiones";
        public static final String FIELD_SESSION = "sesion";
        public static final String FIELD_USERNAME = "username";
    }
}
