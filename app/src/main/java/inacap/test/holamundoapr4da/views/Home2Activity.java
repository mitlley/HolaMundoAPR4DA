package inacap.test.holamundoapr4da.views;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import inacap.test.holamundoapr4da.MainActivity;
import inacap.test.holamundoapr4da.R;
import inacap.test.holamundoapr4da.model.sqlite.HolaMundoDBContract;
import inacap.test.holamundoapr4da.views.fragments.BienvenidaFragment;
import inacap.test.holamundoapr4da.views.fragments.MapaFragment;
import inacap.test.holamundoapr4da.views.fragments.SegundoFragment;

public class Home2Activity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, BienvenidaFragment.OnFragmentInteractionListener, SegundoFragment.OnFragmentInteractionListener, MapaFragment.OnFragmentInteractionListener {

    private MenuItem itemAnterior = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home2);


        // Llamamos  a la barra superior
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Boton flotante, en la parte inferior de la pantalla
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Mensaje temporal numero 2
                Snackbar.make(view, "Yo soy un Snackbar", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // Componente que permite la apertura de un panel lateral
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        // Llamamos al panel lateral
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        // Ponemos el NavegationView (panel lateral) en un modo de escucha
        // Esto actuara cuando la persona seleccione un item del panel lateral
        navigationView.setNavigationItemSelectedListener(this);

        // Obetener la cabecera del panel lateral
        View cabecera = navigationView.getHeaderView(0);

        // Llamar al textView para mostrar el nombre
        TextView tvUsername = (TextView) cabecera.findViewById(R.id.tvUsername);

        // Obtener el nombre de usuario desde las preferencias compartidas
        SharedPreferences sesiones = getSharedPreferences(HolaMundoDBContract.HolaMundoSesion.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        String username = sesiones.getString(HolaMundoDBContract.HolaMundoSesion.FIELD_USERNAME, "NOMBREUSUARIO");

        // Mostrar el nombre
        tvUsername.setText(username);
        navigationView.setCheckedItem(R.id.nav_share);
        navigationView.getMenu().performIdentifierAction(R.id.nav_share, 0);
    }

    @Override
    public void onBackPressed() {

        // Controlamos el comportamiento del boton 'atras'

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        // Si el panel lateral esta abierto
        if (drawer.isDrawerOpen(GravityCompat.START)) {

            // Lo cerramos
            drawer.closeDrawer(GravityCompat.START);
        } else {

            // de lo contrario, actuamos de forma normal
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        // Controlar las acciones del menu superior


        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_logout){
            // Debemos manejar la salida o cierre de sesion

            SharedPreferences sesiones = getSharedPreferences(HolaMundoDBContract.HolaMundoSesion.SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sesiones.edit();

            editor.putBoolean(HolaMundoDBContract.HolaMundoSesion.FIELD_SESSION, false);
            editor.putString(HolaMundoDBContract.HolaMundoSesion.FIELD_USERNAME, "");
            editor.commit();

            Intent i = new Intent(Home2Activity.this, MainActivity.class);
            startActivity(i);
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        // Esta funcion se ejecuta cuando la persona selecciona un item del panel lateral

        // Handle navigation view item clicks here.

        // Fragmento a mostrar
        Fragment fragment = null;
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
            fragment = new SegundoFragment();

        } /*else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        }*/ else if (id == R.id.nav_share) {
            fragment = new MapaFragment();
        } else if (id == R.id.nav_send) {

        }

        if(fragment != null){

            // Remplazar el fragment
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.flContent, fragment).commit();

        }

        // Consultar si no es el primer item en ser seleccionado
        if(itemAnterior != null){
            itemAnterior.setChecked(false);
        }

        // Destacar el item seleccionado
        item.setChecked(true);

        // Cambiat el titulo de la ventana
        setTitle(item.getTitle());

        itemAnterior = item;

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(String fragmentName, String action) {
        if(fragmentName.equals("BienvenidaFragment")){
            if(action.equals("CONTADOR")){
                Toast.makeText(getApplicationContext(), "Contador incrementado", Toast.LENGTH_SHORT).show();
            }
        }
    }
}

























